#include <stdio.h>

int solution(int A[], int N){
	int B[N];
	int bIndex = 0;
	int result = 0;

	for (int i = 0; i < N; i++) {
		B[i] = 0;
	}
	
	for (int i = 0; i < N; ++i) {
		int status = 0;

		for (int j = 0; j < N; j++) {
			if (B[j] == A[i]){
				status = 1;
				break;
			}
		}

		if (status == 0)
			result++;
			B[bIndex++] = A[i];
	}

	return result;
}

int main(){
	int soal[] = {1, 2, 3, 1, 2, 1, 1};
	int soalLen = 7;

	int distinc = solution(soal, soalLen);
	printf("distinc %d\n", distinc);
}